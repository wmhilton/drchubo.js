TO INSTALL
==========

You will need an Internet connection to complete the install process.

Prerequisites: an installation of hubo-ach and [node][].

* `git clone --recursive https://bitbucket.org/wmhilton/drchubo.js.git`
    * You may be prompted to enter your credentials twice, because of the private drchubo-urdf submodule.
* `cd drchubo.js`
* `./install_all.sh`
    * Note that this will try to copy files to /var/www/hubo. If you don't want this, run the steps in `install_all.sh` by hand.


About DRC-Hubo-in-the-Browser
=============================

This is a enhanced version of Hubo-in-the-Browser done for the DARPA Robotics Challenge competition.
Please see the original open source project, <http://github.com/wmhilton/hubo-js> for more information 
about the code.
